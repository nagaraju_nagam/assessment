import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})

export class UserServiceService {
  url = "http://localhost:3000/"
  constructor(private http: HttpClient) {

  }

  getItemsList(): Observable<any> {
    return this.http.get(this.url + "getItems");
  }

  loginUser(data): Observable<any> {
    console.log(data);
    return this.http.post(this.url + "login", data);
  }
  registerUser(data): Observable<any> {
    return this.http.post(this.url + "register", data);
  }
  manageUserData(): Observable<any> {
    return this.http.get(this.url + "manage");
  }

  deleteUserData(id): Observable<any> {
    return this.http.get(this.url + "delete/" + id)
  }

  getSpecificUserData(id): Observable<any> {
    return this.http.get(this.url + "getUserData/" + id);
  }

  updateSpecificUserData(data): Observable<any> {
    return this.http.put(this.url + "updatedata", data);
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }

    });
  }

}
