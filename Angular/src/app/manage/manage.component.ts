import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../Services/user-service.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class ManageComponent implements OnInit {
  userData: Array<any> = [];
  closeResult: string;
  display: boolean = false;
  succMsg: boolean = false;
  msgText: String = "";

  editForm: FormGroup;
  itemData: any = "";
  count: number = 0;
  numbers: Array<Number> = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  constructor(private userService: UserServiceService,
    private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.editForm = this.fb.group({
      'id': [''],
      'emailid': ['', Validators.required],
      'firstname': ['', Validators.required],
      'lastname': ['', Validators.required],
      'address1': ['', Validators.required],
      'address2': ['', Validators.required],
      'phoneno': ['', Validators.required],
      'photourl': ['']
    });

    this.getallItemsList();
  }

  getallItemsList() {
    this.userService.getItemsList().subscribe(itemRes => {
      this.itemData = itemRes;
    })
  }

  getUserData() {
    this.userService.manageUserData().subscribe(manageRes => {
      this.userData = manageRes;
    })
  }



  getSpecificUserData(id) {
    this.display = true;
    this.userService.getSpecificUserData(id).subscribe(userRes => {
      console.log(userRes);
      this.editForm.controls['id'].setValue(userRes[0].id);
      this.editForm.controls['firstname'].setValue(userRes[0].firstname);
      this.editForm.controls['lastname'].setValue(userRes[0].lastname);
      this.editForm.controls['emailid'].setValue(userRes[0].emailid);
      this.editForm.controls['address1'].setValue(userRes[0].address1);
      this.editForm.controls['address2'].setValue(userRes[0].address2);
      this.editForm.controls['phoneno'].setValue(userRes[0].phoneno);
      // this.base64textString = "http://localhost:3000/uploads/" + userRes[0].photourl;
    })
  }


  deleteUserData(id) {
    this.userService.deleteUserData(id).subscribe(res => {

      this.succMsg = true;
      this.msgText = "User deleted";
      console.log("res", res);
      setTimeout(() => {
        this.succMsg = false;
        this.msgText = "";
        this.getUserData();
      }, 3000)
    })
  }



  updateData() {
    if (this.editForm.valid) {
      let data = this.editForm.value;
      // data.photourl=this.base64textString;
      // let extensin = this.imgName.split('.')
      // data.profilepic = extensin[0] + '-' + Date.now() + '.' + extensin[1];
      // data.photourl = this.base64textString.split('base64,')[1];
      console.log(data.photourl)
      this.userService.updateSpecificUserData(this.editForm.value).subscribe(updateRes => {
        console.log(updateRes);
        this.succMsg = true;
        this.msgText = "User data Updated ";
        setTimeout(() => {
          this.succMsg = false;
          this.msgText = "";
          this.getUserData();
          this.display = false;
        }, 3000)
      })
    } else {
      this.userService.validateAllFormFields(this.editForm);
    }

  }



  logout() {
    sessionStorage.clear();
    localStorage.clear();
    this.router.navigateByUrl('/');
  }

  getStoreValues(event){
    console.log(event);
  }
  addToCart(itemdata,c) {
    console.log(itemdata,c);
  }


}
