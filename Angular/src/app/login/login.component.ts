import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserServiceService } from '../Services/user-service.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  registrationForm: FormGroup;
  loginForm: FormGroup;
  selectedFile: File;
  login: boolean = false;
  register: boolean = true;
  userExists: boolean = false;
  filesToUpload: Array<File>;




  base64textString;
  loginSuceess: boolean = false;
  msgText: String = "";
  imgName: String = "";

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private userservice: UserServiceService
  ) { }

  ngOnInit() {


    this.checkUserAlreadyExistsorNot();

    this.registrationForm = this.fb.group({
      'emailid': ['', Validators.required],
      'username': ['', Validators.required],
      'password': ['', Validators.required],
      'confirmpassword': ['', Validators.required],
      'firstname': ['', Validators.required],
      'lastname': ['', Validators.required],
      'address1': ['', Validators.required],
      'address2': ['', Validators.required],
      'phoneno': ['', Validators.required]
    });

    this.loginForm = this.fb.group({
      'username': ['', [Validators.required]],
      'password': ['', Validators.required],
      'rememberme': ['']
    });


  }


  checkUserAlreadyExistsorNot() {
    if ((localStorage.getItem('username') != null || localStorage.getItem('username') != undefined || localStorage.getItem('username') != '') && (localStorage.getItem('password') != null || localStorage.getItem('password') != undefined || localStorage.getItem('password') != '')) {

      this.loginSuceess = false;
      this.router.navigate(['/manage']);
    }
  }

  registerPopup() {

    this.login = false;
    this.register = true;
  }

  loginPopup() {
    this.login = true;
    this.register = false;
  }

  loginData() {
    console.log(JSON.stringify(this.loginForm.value));
    let data = this.loginForm.value;
    if (this.loginForm.valid) {
      this.userservice.loginUser(data).subscribe(loginRes => {
        console.log(loginRes);
        if (loginRes.length > 0) {
          if (data.rememberme) {
            localStorage.setItem('username', data.username);
            localStorage.setItem('password', data.password);
            this.loginSuceess = false;
            this.router.navigate(['/manage']);
          } else {
            this.loginSuceess = false;
            this.router.navigate(['/manage']);
            sessionStorage.setItem('username', data.username);
            sessionStorage.setItem('password', data.password);
          }


        } else {
          this.loginSuceess = true;
          this.msgText = "Invalid Username/Password";
        }
        console.log(loginRes, "  login");
      })
    } else {
      this.userservice.validateAllFormFields(this.loginForm);
    }
  }


  registerData() {
    if (!this.userExists && this.registrationForm.valid && this.base64textString != undefined) {
      let data = this.registrationForm.value;
      // let extensin=this.imgName.split('.')
      // data.profilepic = extensin[0] + '-' + Date.now() + '.' + extensin[1];
      // data.photourl = this.base64textString.split('base64,')[1];
      this.userservice.registerUser(this.registrationForm.value).subscribe(registerRes => {
        console.log(registerRes, "  register");
        this.loginSuceess = true;
        this.msgText = "User Details Saved";
        setTimeout(() => {
          this.loginSuceess = false;
          this.msgText = "";
          this.loginPopup();
        }, 3000)
      })
    } else {

      this.checkConfirmPassword();
      this.userservice.validateAllFormFields(this.registrationForm);
    }
  }

  checkConfirmPassword() {
    console.log("in funcs")
    this.userExists = false;
    if ((this.registrationForm.controls['password'].value != '' && this.registrationForm.controls['confirmpassword'].value != '') && (this.registrationForm.controls['password'].value === this.registrationForm.controls['confirmpassword'].value)) {
      this.userExists = false;
      console.log("in if")
    } else {
      this.userExists = true;
      console.log("in ellse")
    }
  }


  // fileChangeEvent(evt: any) {
  //   const file = evt.target.files[0];
  //   this.imgName=file.name;
  //   if (file) {
  //     const reader = new FileReader();

  //     reader.onload = this.handleReaderLoaded.bind(this);
  //     reader.readAsBinaryString(file);
  //   }
  // }

  // handleReaderLoaded(e) {
  //   this.base64textString=('data:image/png;base64,' + btoa(e.target.result));
  //   console.log(this.base64textString);
  // }







}
