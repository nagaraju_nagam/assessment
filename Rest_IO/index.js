const express = require('express')
const app = express();
const port = 3000
const mysql = require('mysql');
var bodyParser = require('body-parser');
var cors = require('cors');
var multer = require('multer');
var path = require('path');
var fs=require('fs');

//CORS-enabled
app.use(cors());

app.use(bodyParser.json({ type: 'application/json',limit: '50mb' }));

app.use(bodyParser.urlencoded({ extended: true }));
app.use('/uploads',express.static('uploads'));



const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'testing'
});

// connect to database
db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database');
});
global.db = db;


app.post("/upload", multer({ dest: "./uploads/" }).array("uploads", 12), function (req, res) {
    console.log(req);
    res.send(req.files);
});


app.post('/register', (req, res) => {
   
    let regData = req.body;

    let data = req.body;

    console.log(typeof data.photourl)
    let bufferData=Buffer.from(data.photourl,'base64');
    console.log('------------',bufferData)
    let uploadFile=fs.writeFile('uploads/'+data.profilepic,bufferData,(err)=>{
    if(err) console.log('-- error while writing the file --',err);
    console.log('--- file has uploaded successfully --');
    });


    let query = "insert into loginmst (firstname,lastname,username,emailid,phoneno,address1,address2,password,photourl) values ('" +
        regData.firstname + "','" +
        regData.lastname + "','" +
        regData.username + "','" +
        regData.emailid + "','" +
        regData.phoneno + "','" +
        regData.address1 + "','" +
        regData.address2 + "','" +
        regData.password + "','" +
        regData.profilepic + "')";
    db.query(query, (err, result) => {
        console.log(err, " err");
        if (err) {
            res.redirect('/');
        }
        console.log(result);
        res.send(result);
    });

});

app.post('/login', (req, res) => {
    console.log(req.body);

    let query = "select * from loginmst where username='" + req.body.username + "' and password='" + req.body.password + "';"
    db.query(query, (err, result) => {
        console.log(err);
        if (err) {
            res.redirect('/');
        }
        console.log(result);
        res.send(result);
    });


    console.log(req.body);
});


app.get('/getUserData/:id', function (req, res) {
    console.log(req.body);

    let query = "select * from loginmst where id='" + req.params.id + "';"
    db.query(query, (err, result) => {
        if (err) {
            res.redirect('/');
        }
        console.log(result);
        res.send(result);
    });

});



app.get('/manage', function (req, res) {
    console.log(req.body);
    let query = "select * from loginmst;"
    db.query(query, (err, result) => {
        if (err) {
            res.redirect('/');
        }
        console.log(result);
        res.send(result);
    });

})





app.get('/getItems', function (req, res) {
    console.log(req.body);
    let query = "select * from itemmst;"
    db.query(query, (err, result) => {
        if (err) {
            res.redirect('/');
        }
        console.log(result);
        res.send(result);
    });

})



app.put('/updatedata', (req, res) => {
    let data = req.body;

    console.log(typeof data.photourl)
    let bufferData=Buffer.from(data.photourl,'base64');
    console.log('------------',bufferData)
    let uploadFile=fs.writeFile('uploads/'+data.profilepic,bufferData,(err)=>{
    if(err) console.log('-- error while writing the file --',err);
    console.log('--- file has uploaded successfully --');
    });


    let query = "update loginmst set firstname='" + data.firstname + "',lastname='" + data.lastname + "',emailid='" + data.emailid + "',phoneno='" + data.phoneno + "',address1='" + data.address1 + "',address2='" + data.address2 + "',photourl='" + data.profilepic + "' where id=" + data.id + ";"
    db.query(query, (err, result) => {
        console.log(err, " err");
        if (err) {
            res.redirect('/');
        }
        console.log(result);
        res.send(result);
    });

})




app.get('/delete/:id', function (req, res) {
    console.log(req.params.id);
    let query = "delete from loginmst where id=" + req.params.id + ";"
    db.query(query, (err, result) => {
        if (err) {
            res.redirect('/');
        }
        console.log(result);
        res.send(result);
    });

})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))